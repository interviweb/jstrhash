const strHashCode = require('./index');

console.assert(strHashCode('test string') === -318923937);
console.assert(strHashCode('') === 0);
console.assert(strHashCode(1) === 0);
