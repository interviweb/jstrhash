# JStrHash

This library very easy and lightweight, provides string hashCode, similar into Java. For browser and Node.

## Installing

`npm i jstrhash`

## Usage

```
const strHashCode = require('jstrhash');

let hash = strHashCode('mystring');
console.log(hash);
```
