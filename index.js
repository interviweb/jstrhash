/**
 * Returns a hash code for this string. The hash code for a String object is computed as
 * s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]
 * The hash value of the empty string, null or other type is zero.
 * @param {string} str
 * @return {number}
 */
function strHashCode(str) {
    if (!str || typeof(str) !== 'string') return 0;
    let result = 0;
    for (let i = 0; i < str.length; i++) {
        result = (result << 5) - result + str.charCodeAt(i) | 0;
        result = result & result;
    }
    return result;
}

if (module && module.exports) {
    module.exports = strHashCode;
}
